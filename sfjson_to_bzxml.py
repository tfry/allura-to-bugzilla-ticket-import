#!/usr/bin/python
# ***************************************************************************
#                          sfjson_to_bzxml.py  -  description
#                             -------------------
#    begin                : Jan 2016
#    copyright            : (C) 2016 by Thomas Friedrichsmeier
#    email                : thomas.friedrichsmeier@kdemail.net
# ***************************************************************************
#
# ***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************
#
# Takes the JSON dump of a Sourceforge Allura ticket tracker, and converts it
# into an xml-dump for importing in Bugzilla. Attachments are fetched from SF
# and embedded into the XML in base64 encoding.
#
# Tested with bugzilla 4.4.10, may need adjustments for other versions.
#
# Coding style is rather quick dirty all in all. Esp. things would be cleaner, if there
# was an abstract (class-based) layer between SF-format and BZ-format, rather than
# converting between the two on the fly. But this is / was a one-shot project for me.
# Feel free to rewrite ;-).
#
# Usage: python sfjson_to_bzxml.py JSONdump exportdir
#
# You will also want to go through the top parts of this script and adjust parameters.
#
# You may also want to modify Bugzilla's importxml.pl to keep it from appending
# and additional comment to each imported bug.

import xml.etree.ElementTree as ET
import json
import sys
import os
import urllib
import datetime
import mimetypes

DEFAULT_ASSIGNEE = { "name" : "RKWard Team", "email" : "rkward-devel" + "@" + "kde.org" }
DEFAULT_USER = DEFAULT_ASSIGNEE
AUTHOR_MAP = {
    'tfry' : { "name": "Thomas Friedrichsmeier", "email" : "thomas.friedrichsmeier" + "@" + "ruhr-uni-bochum.de" }
    # Add any other authors with a known mapping from SF-id to BZ-id.
    # Items authored by any others will be imported as "authored" by the DEFAULT_USER. A comment revealing the real author is added in this case.
}
DEFAULT_PRODUCT = "rkward"
DEFAULT_COMPONENT = "general"
DEFAULT_SEVERITY = "wishlist"
BASE_URL = 'http://sourceforge.net/p/rkward/bugs/'   # For adding back-links from the imported ticket to the original one

# config.py can be used to customize/overwrite the above settings, without confusing git with your sensitive data
if os.path.exists ("config.py"):
  execfile ("config.py")

def parse_sfts(ts):
    #2013-08-10 23:36:31
    fmt = "%Y-%m-%d %H:%M:%S"
    if ('.' in ts):
        #2013-08-10 23:36:31.812000
        fmt += ".%f"
    return datetime.datetime.strptime(ts, fmt)

def print_bzts(ts):
    return ts.strftime ("%Y-%m-%d %H:%M:%S") + " +0100"  #HACK

def escape (string):
    return string

def find_status(sf_status):
    if sf_status.startswith('closed'):
        return "RESOLVED"
    return "UNCONFIRMED"

def find_resolution(sf_status):
    if sf_status.startswith('closed'):
        if sf_status.endswith('fixed'):
            return "FIXED"
        if sf_status.endswith('invalid'):
            return "INVALID"
        if sf_status.endswith('out-of-date'):
            return "WORKSFORME"
        if sf_status.endswith('works-for-me'):
            return "WORKSFORME"
        if sf_status.endswith('duplicate'):
            return "DUPLICATE"
        return "FIXED"
    return None

def map_author(sf_author):
    note = ""
    author = DEFAULT_ASSIGNEE
    if (sf_author in AUTHOR_MAP):
        author = AUTHOR_MAP[sf_author]
    else:
        note = "-- Originally posted by (AT sourceforge.net): {0} --\n".format (sf_author)
    return (author, note)

def create_bug(create_ts, bugid, summary, description, sf_author, sf_status, attachments):
    global bugids
    (author, note) = map_author (sf_author)
    if bugid in bugids:
        print ("Error: Duplicate bug id {}. Convert trackers separately!".format (bugid))
        exit ()
    bugids.append (bugid)
    bug = ET.fromstring ('''<bug>
        <bug_id>{0}</bug_id>
        
        <creation_ts>{1}</creation_ts>
        <short_desc></short_desc>
        <delta_ts>{1}</delta_ts>
        <reporter_accessible>1</reporter_accessible>
        <cclist_accessible>1</cclist_accessible>
        <classification_id>1</classification_id>
        <classification>Unclassified</classification>
        <product>{2}</product>
        <component>{3}</component>
        <version>unspecified</version>
        <rep_platform>unspecified</rep_platform>
        <op_sys>All</op_sys>
        <bug_status>UNCONFIRMED</bug_status>
        
        <bug_file_loc/>
        <status_whiteboard/>
        <keywords/>
        <priority>NOR</priority>
        <bug_severity>{4}</bug_severity>
        <target_milestone>---</target_milestone>
        
        
        <everconfirmed>0</everconfirmed>
        <reporter name="{5}">{6}</reporter>
        <assigned_to name="{7}">{8}</assigned_to>
        
        <estimated_time>0.00</estimated_time>
        <remaining_time>0.00</remaining_time>
        <actual_time>0.00</actual_time>

        </bug>'''.format (bugid, print_bzts (create_ts), DEFAULT_PRODUCT, DEFAULT_COMPONENT, DEFAULT_SEVERITY, author['name'], author['email'], DEFAULT_ASSIGNEE['name'], DEFAULT_ASSIGNEE['email']))
    bug.find ('short_desc').text = escape (summary)
    bug.append (create_comment (create_ts, description, sf_author, attachments))
    status = find_status (sf_status)
    if (status != "UNCONFIRMED"):
        bug.find ('bug_status').text = status
        bug.find ('everconfirmed').text = "1"
        ET.SubElement (bug, 'resolution').text = find_resolution (sf_status)
    return bug

def create_comment(create_ts, description, sf_author, attachments):
    (author, note) = map_author (sf_author)
    description = note + description
    comment = ET.fromstring ('''
        <long_desc isprivate="0">
        <who name="{0}">{1}</who>
        <bug_when>{2}</bug_when>
        <thetext></thetext>
        </long_desc>'''.format (author['name'], author['email'], print_bzts (create_ts)))
    for (attid, element) in attachments:
        description = description + "\n-- Created an attachment --"    # Note no point in specifying the attachment id: It will be re-indexed on import...
        ET.SubElement (comment, 'attachid').text = str (attid)
    comment.find ('thetext').text = escape (description)
    return comment

def create_attachments(json_list, create_ts, description, sf_author):
    global attachid
    (author, note) = map_author (sf_author)
    description = note + description[0:100]  # SF does not have the concept of attachment-descriptions. Instead we use the first 100 chars of the containing post's description, which is usally what we need.
    ret = []
    for json_attach in json_list:
        attachid = attachid + 1
        url = json_attach['url']
        file_name=url.rsplit('/', 1)[-1]
        downloaded = os.path.join (outdir, file_name)
        urllib.urlretrieve (url, downloaded)
        attachment = ET.fromstring ('''
            <attachment isobsolete="0" ispatch="0" isprivate="0">
            <attachid>{0}</attachid>
            <date>{1}</date>
            <delta_ts>{1}</delta_ts>
            <desc></desc>
            <filename>{2}</filename>
            <data encoding="base64"/>
            <type>text/plain</type>
            <attacher name="{3}">{4}</attacher>
            </attachment>'''.format (attachid, print_bzts (create_ts), file_name, author['name'], author['email']))
        attachment.find ('desc').text = escape (description)
        (mtype, dummy) = mimetypes.guess_type (downloaded)
        if (mtype):
            attachment.find ('type').text = mtype
        with open(downloaded, "rb") as f:
            data = f.read()
            attachment.find ('data').text = data.encode("base64")
        os.remove (downloaded)
        ret.append ((attachid, attachment))
    return ret

bugids = []
attachid = 0
indoc = json.load (open (sys.argv[1]))
outdir = sys.argv[2]
tickets = indoc['tickets']

outdoc = ET.fromstring (
    '''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <!DOCTYPE bugzilla SYSTEM "http://localhost/page.cgi?id=bugzilla.dtd">
    <bugzilla version="4.4.10" urlbase="http://localhost/" maintainer="{0}" exporter="{0}">
    </bugzilla>
    '''.format (DEFAULT_USER['email']))

for ticket in tickets:
    lowest_ts = parse_sfts (ticket['created_date'])  # Note: Esp. with attachments, we can't always rely on finding the most recent modification last.
    highest_ts = lowest_ts                           #       so we keep track of oldest and youngest modification for bug start and mod date.

    # first deal with attachments that are attached to the bug itself (created, with the report), because we have to amend the initial description for this
    attachments = create_attachments (ticket['attachments'], lowest_ts, ticket['summary'], ticket['reported_by'])

    description = "-- This ticket was imported from {0} on {1} --\n".format (BASE_URL + str (ticket['ticket_num']), print_bzts (datetime.datetime.now ())) + ticket['description']
    if (len (ticket['labels'])):
        description += "-- Labels: {0} --\n".format (', '.join (ticket['labels']))
    bug = create_bug (lowest_ts, ticket['ticket_num'], ticket['summary'], "\n" + description, ticket['reported_by'], ticket['status'], attachments)
    if ('discussion_thread' in ticket):
        for post in ticket['discussion_thread']['posts']:
            ts = parse_sfts (post['timestamp'])
            if (ts < lowest_ts):
                print ('Ooops')
                die ()
            if (ts > highest_ts):
                highest_ts = ts
            post_attachments = create_attachments (post['attachments'], ts, post['text'], post['author'])
            attachments = attachments + post_attachments
            bug.append (create_comment (ts, post['text'], post['author'], post_attachments))
            #add_attachments (post['attachments'])

    for (attid, element) in attachments:
        bug.append (element)
    
    bug.find ('delta_ts').text = print_bzts (highest_ts)
    outdoc.append (bug)

ET.ElementTree (outdoc).write(os.path.join (outdir, "exported_tickets.xml"))
